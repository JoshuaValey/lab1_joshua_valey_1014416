#include "pch.h"

using namespace System;

int main(array<System::String ^> ^args)
{

	try {
		Console::WriteLine(L"Ingrese la cantidad de numeros de la secuencia Fibonacci que desea");
		int numero = Convert::ToInt32(Console::ReadLine());
		Console::WriteLine(Fibonacci(numero, 0, 1));

		Console::WriteLine("Ingrese la cantidad de numeros de la secuencia Fibonacci que desea (cola)");
		numero = Convert::ToInt32(Console::ReadLine());
		Fibonacci(numero, 0, 1, "");

		Console::WriteLine(L"Ingrese un numero para calcular el factorial");
		numero = Convert::ToInt32(Console::ReadLine());
		Console::WriteLine(factorial(numero));

		Console::WriteLine("Ingrese un numero para calcular el factorial (cola)");
		numero = Convert::ToInt32(Console::ReadLine());
		factorial(numero, 1);

		Console::WriteLine("Ingrese un numero en base 10 para convertir a binario");
		numero = Convert::ToInt32(Console::ReadLine());
		Console::WriteLine(Binario(numero, 1));
	}
	catch (...) {
		Console::WriteLine("Error en el ingreso de datos");
	}


	Console::ReadKey();

    return 0;
}


//Fibonacci recursivo
String^ Fibonacci(int n, int a, int b) {

	//Solamente se permiten numeros mayores a 1
	if (n <= 0) {
		throw gcnew Exception("Valor incorrecto, solo enteros mayores que 0");
	}

	if (n == 1) {
		if (a == 0)
			return "1";
		else
			return " ";
	}

	else if (n > 1) {
		if (a == 0)
			return "1, " + (a + b) + ", " + Fibonacci((n - 1), b, (a + b));
		else
			return "" + (a + b) + ", " + Fibonacci((n - 1), b, (a + b));
	}
}

//Fibonacci recurisivo con cola
void Fibonacci(int n, int a, int b, String^ salida) {
	if (n <= 0) {
		throw gcnew Exception("Valor incorrecto, solo enteros mayores que 0");
	}

	if (n == 1) {
		if (a == 0)
			Console::WriteLine("1");
		else
			Console::WriteLine(salida);
	}
	else {
		if (a == 0)
			salida += "1, " + (a + b) + ", ";
		else
			salida += "" + (a + b) + ", ";

		Fibonacci((n - 1), b, (a + b), salida);
	}
}



int factorial(int n) {
	if (n <= 1)
		return 1;
	else
		return n * factorial(n - 1);
}

void factorial(int n, int result) {
	if (n <= 1)
		Console::WriteLine("" + result);
	else
		factorial(n - 1, result *= n);
}



int Binario(int n, int factor) {
	if (n == 1) {
		return n * factor;
	}
	else {
		return Binario(n / 2, factor * 10) + (n % 2) * factor;
	}
}